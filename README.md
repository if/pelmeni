# pelmeni

![pic of pelmeni with project motto](media/propaganda/logo.png)
![pic of pelmeni with project motto](media/propaganda/logo-(9).png)


Highly experimental middleware that provides a simplified interface to
[pleroma](https://pleroma.social/)'s API, using the 9P2000 protocol.


## Contributing
Contributions are generally welcome, especially if you want to test
pelmeni on different operating systems or work on translating
the documentation into another language.

Pelmeni has been tested on the following operating systems:
- Debian stable

Documentation is available in the following languages:
- English


## For more information...
... read the [manual](./manual).


## License
BSD3
