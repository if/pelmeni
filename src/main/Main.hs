{-# LANGUAGE OverloadedStrings #-}

module Main where

import Pelmeni.Config
import Pelmeni.Server
import System.Environment (getArgs)


main :: IO ()
main = do
  args <- getArgs
  case args of
    [fpath] -> startServer `withConfigFile` fpath
    _ -> putStrLn "usage: ./pelmeni PATH_TO_CONFIG"

-- | Try to parse a configuration file.  If parsing
-- fails, print an error message; if it succeeds,
-- pass it to a continuation.
withConfigFile :: (Config -> IO ()) -> FilePath -> IO ()
withConfigFile k fpath =
  parseConfig fpath >>= either putStrLn k
