-- | Common types used across modules in Pelmeni.Server.
module Pelmeni.Server.Types where

import Control.Monad.State.Lazy (evalStateT)
import Control.Monad.Reader (ReaderT, runReaderT)

import Data.Word (Word8, Word16, Word32)

import qualified Pelmeni.Config as C
import Pelmeni.Server.Filesys



-- some basic typedefs for making type signatures clearer.
-- consider making these `newtype's later on.
type TagNum = Word16
type Size = Word32
type Count = Word32
type Version = String
type Mode = Word8

type Server a = FilesysT (ReaderT C.Config IO) a

runServer :: C.Config -> Filesys -> Server a -> IO a
runServer cfg fsys s = runReaderT (evalStateT s fsys) cfg


data Scroll = Scroll ScrollDirection ScrollMode
  deriving (Eq, Ord, Show)
data ScrollDirection = SForward | SBackward
  deriving (Eq, Ord, Show)
data ScrollMode = SRewrite | SAppend
  deriving (Eq, Ord, Show)
