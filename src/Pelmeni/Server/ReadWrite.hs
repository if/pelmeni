{-# LANGUAGE TupleSections #-}


-- | Utilities for doing the (rather gnarly) work of reading from
-- and writing to files in the 9p server's synthetic file system.
module Pelmeni.Server.ReadWrite where

import Pelmeni.Pleroma as P
import Pelmeni.Server.Util
import Pelmeni.Server.Parse
import Pelmeni.Server.Types
import Pelmeni.Server.Filesys
import Pelmeni.Server.Path
import Pelmeni.Server.Tree

import Control.Monad ((>=>), when)
import Control.Monad.IO.Class (liftIO)

import Data.Aeson (FromJSON)
import Data.Binary.Put (runPut)
import Data.Bits
import qualified Data.ByteString.Lazy as B (ByteString, concat)
import Data.Maybe (catMaybes, maybe)
import Data.NineP

import System.Directory
  ( doesDirectoryExist
  , doesFileExist
  , listDirectory )




-- TODO: validate input; see man pages for more info
walk :: FPath Absolute -> FPath Relative -> Fid -> Server [Qid]
walk fp wpath nfid = do
  let r = route wpath fp
  w <- walk' r
  let (fids, qids) = unzip w
  when (length w == length r)
    (putFid nfid (lastMaybe fp fids))
  return qids
  where walk' = mapMaybeM (\p -> fmap (p,) <$> step p)
        -- turn each part of the walk path `wp' into an
        -- absolute path with respect to absolute path fp
        route :: FPath Relative -> FPath Absolute -> [FPath Absolute]
        route wp anchor =
          -- `tail' is used to ignore /, because when wp is empty,
          -- relroutes should also be empty.
          let relroute = tail (routeTo wp)
          in map (`absoluteFrom` anchor) relroute
        step (FPath ["people", p]) = makePerson p
        step (FPath ["statuses", s]) = fetchStatus s
        step p = getFPathQid p


-- | Try to make a directory for the fediverse user with
-- the given handle.  If successful, this function returns
-- a Qid representing their directory.
makePerson :: String -> Server (Maybe Qid)
makePerson name =
  getFPathQid (FPath ["people", name])
  >>= maybe initPerson (return . Just)
  where
    initPerson = do
      let req = getAccount (ID name) False
      acct <- pleromaReq req
      case acct of
        Nothing -> return Nothing
        Just  a ->
          toTree a >>= addTree (FPath ["people"])

-- TODO: good candidate for refactoring.
fetchStatus :: String -> Server (Maybe Qid)
fetchStatus statusid =
  getFPathQid (FPath ["statuses", statusid])
  >>= maybe initStatus (return . Just)
  where
    initStatus = do
      let req = getStatus (ID statusid)
      mstatus <- pleromaReq req
      case mstatus of
        Nothing -> return Nothing
        Just  s ->
          toTree s >>= addTree (FPath ["statuses"])



reqWithParse :: (FromJSON b, ParseTo a)
             => (a -> P.Action b) -> String -> Server ()
reqWithParse f s =
  case parseTo s of
    Nothing -> return ()
    Just a ->
      pleromaReq (f a)
      >> return ()
    

star, unstar, reblog, unblog, react, unreact, vote
  :: String -> Server ()
star = reqWithParse favoriteStatus
unstar = reqWithParse unfavoriteStatus
reblog = reqWithParse repeatStatus
unblog = reqWithParse unrepeatStatus
react = reqWithParse (uncurry emojiReact)
unreact = reqWithParse (uncurry rmReaction)
vote = reqWithParse (uncurry voteOnPoll)


post :: String -> Server ()
post s = case parseTo s of
  Nothing   -> return ()
  Just args -> post' args

post' :: (String, ID P.Status) -> Server ()
post' (postdir, reply) = do
  dirExists <- liftIO (doesDirectoryExist postdir)
  when dirExists $ do
    s <- liftIO readPost
    media <- fetchMedia
    let req = postStatus reply "" s media P.Unlisted False
    _ <- pleromaReq req
    return ()
  where
    readPost :: IO String
    readPost = do
      statp <- doesFileExist fpstatus
      if statp then readFile fpstatus
        else return ""
    fetchMedia :: Server [String]
    fetchMedia = do
      medp <- liftIO (doesDirectoryExist fpmedia)
      if medp then do
        fps <- liftIO (listDirectory fpmedia)
        let fps' = map (fpmedia++) fps
        ms <- fmap catMaybes (mapM upload fps')
        return (map (fromID . getID) ms)
        else return []
    fpmedia = postdir ++ "/media/"
    fpstatus = postdir ++ "/status"
    upload :: String -> Server (Maybe P.Attachment)
    upload = pleromaReq . uploadMedia
          


-- TODO: use MaybeT to unwrap all the Maybe values
-- (and when you do, test invalid input)
tlScroll :: (FromJSON a, ToTree a, HasID a)
         => FPath Absolute -> FPath Absolute
         -> (IDRange a -> Action [a])
         -> (String -> Server ())
tlScroll fp store tl sscroll = do
  Just contents <- fmap (fmap cl2utf8) (readFPath fp)
  let (firstID, lastID) = getLimits contents
      mscroll = parseTo sscroll
  case mscroll of
    Nothing -> return ()
    Just scroll -> do
      let range = case scroll of
            Scroll SForward  _ -> (ID firstID, ID     "")
            Scroll SBackward _ -> (ID      "", ID lastID)
      Just items <- pleromaReq (tl range)
      mapM_ (toTree >=> addTree store) items
      let contents' = case scroll of
            Scroll _ SRewrite ->
              listIDs items
            Scroll SForward SAppend ->
              colonsep [listIDs items, contents]
            Scroll SBackward SAppend ->
              colonsep [contents, listIDs items]
      writeFPath contents' fp
  where getLimits "" = ("", "")
        getLimits s =
          let ps = split ':' s
          in (head ps, last ps)
      


-- | Generate a bytestring representation of a file.
fileRepr :: FPath Absolute -> Qid -> Server (Maybe B.ByteString)
fileRepr fpath q =
  case qidtype q of
    FDIR -> dirChildren fpath >>= mapM fmtStats
    FNORMAL -> readFPath fpath
    _ -> return Nothing
  where fmtStats fps = do
          stats <- fmap catMaybes (mapM statFPath fps)
          let puts = map (runPut . put) stats
          return (B.concat puts)

-- | Given an fpath and a qid, make a stat.
mkstat :: FPath Absolute -> Qid -> Server Stat
mkstat fp q = do
  mlen <- readFPath fp
  let len = maybe 0 cllen mlen
  return Stat { st_typ = 0
              , st_dev = 0
              , st_qid = q
              , st_mode = mode
              , st_atime = 0
              , st_mtime = 0
              , st_length = len
              , st_name = statpath fp
              , st_uid = ""
              , st_gid = ""
              , st_muid = "" }
  where mode = fromIntegral (qid_typ q) `shift` 24 .|. 0o0444

-- | Create a stat from an fpath.
statFPath :: FPath Absolute -> Server (Maybe Stat)
statFPath fp = getFPathQid fp >>= mapM (mkstat fp)
