module Pelmeni.Server.Util where


import Pelmeni.Server.Types
import qualified Pelmeni.Config as C
import qualified Pelmeni.Pleroma as P

import Control.Monad.Reader (asks)
import Control.Monad.IO.Class (liftIO)

import Data.Aeson (FromJSON)
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as U
import qualified Data.ByteString.Lazy.Char8 as CL
import Data.List (intercalate)


colonsep :: [String] -> String
colonsep = intercalate ":"
    
listIDs :: (P.HasID a) => [a] -> String
listIDs as = colonsep (map (P.fromID . P.getID) as)


-- | Perform a pleroma API request using the server's config.
pleromaReq :: (FromJSON a) => P.Action a -> Server (Maybe a)
pleromaReq req = do
  sess <- getSession
  liftIO (P.performRequest sess req)


getSession :: Server P.Session
getSession = asks mksession
  where mksession cfg =
          P.makeSession (C.instance_ cfg) (C.token cfg)

-- | Convert a boolean to "0" or "1".
strbool :: Bool -> String
strbool False = "0"
strbool  True = "1"


-- from Control.Monad.Extra in package `extra'
-- | Map an action over a list until that action returns a @Nothing@.
mapMaybeM :: (Monad m) => (a -> m (Maybe b)) -> [a] -> m [b]
{-# INLINE mapMaybeM #-}
mapMaybeM op = foldr go (pure [])
    where go x xs = do
            mx <- op x
            case mx of
              Nothing -> xs
              Just x' ->
                do xs' <- xs
                   pure (x':xs')


-- | Get the last element of a list, with a default value.
lastMaybe :: a -> [a] -> a
lastMaybe a [] = a
lastMaybe _ xs = last xs

mcons :: Maybe a -> [a] -> [a]
mcons (Just a) as = a : as
mcons Nothing  as = as


-- | Take a substring of a bytestring.
trimLBS :: (Integral a, Integral b)
        => (a, b) -> CL.ByteString -> CL.ByteString
trimLBS (off, count) s =
 let off' = fromIntegral off
     count' = fromIntegral count
     s' = CL.toStrict s
     trimmed = B.take count' (B.drop off' s')
 in CL.fromStrict trimmed

cl2utf8 :: CL.ByteString -> String
cl2utf8 = U.toString . CL.toStrict

cl2utf8trim :: CL.ByteString -> String
cl2utf8trim = cl2utf8 . CL.init

cllen :: (Integral a) => CL.ByteString -> a
cllen = fromIntegral . CL.length

