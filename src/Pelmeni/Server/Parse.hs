{-# LANGUAGE FlexibleInstances #-}

-- | Various parsing utilities.
module Pelmeni.Server.Parse
  ( ParseTo(..)
  , split ) where

import Pelmeni.Pleroma as P
import Pelmeni.Server.Types

import Text.Read (readMaybe)



-- | Split a list at occurences of some delimiter.
split :: (Eq a) => a -> [a] -> [[a]]
split x = uncurry (:) . foldr go ([], [])
  where go a (cur, acc)
          | a == x = ([], cur:acc)
          | otherwise = (a:cur, acc)


class ParseTo a where
  parseTo :: String -> Maybe a


instance ParseTo (ID P.Status) where
  parseTo s = Just (ID s)

instance ParseTo (ID P.Account) where
  parseTo s = Just (ID s)

-- TODO: make improper types like these into newtypes
instance ParseTo (ID P.Status, String) where
  parseTo s =
    case split ':' s of
      [status, e] -> do
        st <- parseTo status
        return (st, e)
      _ -> Nothing


instance ParseTo Scroll where
  parseTo ">"  = Just (Scroll SForward  SRewrite)
  parseTo "<"  = Just (Scroll SBackward SRewrite)
  parseTo ">>" = Just (Scroll SForward   SAppend)
  parseTo "<<" = Just (Scroll SBackward  SAppend)
  parseTo    _ = Nothing


instance ParseTo (ID P.Status, [Int]) where
  parseTo s =
    case split ':' s of
      (status:ds) -> do
        st <- parseTo status
        xs <- mapM readMaybe ds
        return (st, xs)
      _ -> Nothing

instance ParseTo (String, ID P.Status) where
  parseTo s =
    case split ':' s of
      [path] -> Just (path, ID "")
      [path, reply] -> do
        rid  <- parseTo reply
        return (path, rid)
      _ -> Nothing
