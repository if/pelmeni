-- | Tools for manipulating (very simple) file paths.
module Pelmeni.Server.Path where

import Data.List (inits, intercalate, stripPrefix)



data Relative
data Absolute
newtype FPath a = FPath [String]
  deriving (Eq, Ord, Show)

fproot :: FPath Absolute
fproot = FPath []


-- | Concatenate two paths.
(</>) :: FPath a -> FPath Relative -> FPath a
(FPath p) </> (FPath q) = FPath (p ++ q)


-- turn a path into an absolute path with slashes
joinpath :: FPath a -> String
joinpath (FPath fp) = '/' : intercalate "/" fp

-- | Format a path for output in a 'stat' message.
statpath :: FPath a -> String
statpath (FPath []) = "/"
statpath (FPath xs) = last xs


-- | Make a relative path absolute with respect to
-- an absolute path.  For example,
-- ../foo `absoluteFrom` /usr/share/perl == /usr/foo.
absoluteFrom :: FPath Relative -> FPath Absolute -> FPath Absolute
absoluteFrom (FPath xs) (FPath ys) = FPath (go xs ys)
  where go [] as = as
        go ("..":bs) [] = go bs []
        go ("..":bs) as = go bs (init as)
        go (b:bs) as = go bs (as ++ [b])

-- | Make one absolute path relative to another absolute path.
-- For example, /usr/share/perl `relativeTo` /usr == share/perl.
relativeTo :: FPath Absolute -> FPath Absolute -> FPath Relative
relativeTo (FPath xs) (FPath ys) =
  case stripPrefix ys xs of
    Nothing  -> FPath xs
    Just xs' -> FPath xs'

-- | List the paths that must be visited, in order, to walk
-- to the given path.  For example, the route to a path called
-- /foo/bar/baz would be [/, /foo, foo/bar, foo/bar/baz].
routeTo :: FPath a -> [FPath a]
routeTo (FPath xs) = map FPath (inits xs)
