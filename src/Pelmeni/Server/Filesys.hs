{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}

-- | Tools for managing a synthetic file system.
-- File system operations are wrapped in a FilesysT
-- monad transformer.
module Pelmeni.Server.Filesys where

import Pelmeni.Server.Path

import qualified Control.Monad.State.Lazy as State

import Data.ByteString.Builder (stringUtf8, toLazyByteString)
import qualified Data.ByteString.Lazy.Internal as BL
import qualified Data.Map as Map
import Data.NineP (Qid(..))
import Data.Word (Word32, Word64)



type Fid = Word32
type PathNum = Word64
type Offset = Word64

data QidType = FDIR | FAPPEND | FNORMAL
  deriving (Eq, Show)

qidtype :: Qid -> QidType
qidtype q =
  case qid_typ q of
    0x80 -> FDIR
    0x40 -> FAPPEND
    0x00 -> FNORMAL
    t    -> error ("invalid qid type: " ++ show t)


mkqid :: QidType -> PathNum -> Qid
mkqid qtyp pn =
  let ntyp = qtype2num qtyp
  in Qid{qid_typ=ntyp,qid_vers=0,qid_path=pn}
  where qtype2num FDIR = 0x80
        qtype2num FAPPEND = 0x40
        qtype2num FNORMAL = 0x00



type FilesysT m a = State.StateT Filesys m a

-- | Keeps track of file identifiers (fids and paths).
data Filesys = Filesys { ffids  :: Map.Map Fid (FPath Absolute)
                         -- ^ fid -> fpath mapping
                       , fqids  :: Map.Map (FPath Absolute) Qid
                         -- ^ fpath -> qid mapping

                         -- , ofids  :: Map.Map Fid Offset
                         -- could map fpaths rather than fids
                         -- -- ^ opened fid -> current offset mapping

                       , ffch :: Map.Map (FPath Absolute) [FPath Absolute]
                         -- TODO: it would be useful to automatically set
                         -- up ffch given initial ffqids, and automatically
                         -- update it when adding an fpath.
                         -- ^ fpath -> children of fpath mapping
                       , fcontents :: Map.Map (FPath Absolute) BL.ByteString
                         -- ^ contents of (non-directory) files.
                       , npaths :: Word64
                         -- ^ number of unique qid paths
                       }


-- While these assumptions may seem a bit arbitrary, this module is not
-- meant to be a library, so we can afford to make it this specific.
-- | Set up a new file system, given an alist of path-to-qid bindings.
-- Note that in the bindings, qid path numbers are assumed to be ordered
-- sequentially, starting at zero.  Also, all paths are assumed to represent
-- directories.
initFilesys :: [(FPath Absolute, Qid)]
            -> [(FPath Absolute, [FPath Absolute])]
            -> [(FPath Absolute, BL.ByteString)]
            -> Filesys
initFilesys qids chs contents =
      Filesys { ffids = Map.empty
              , fqids = Map.fromList qids
              -- , ofids = Map.empty
              , ffch  = Map.fromList chs
              , fcontents = Map.fromList contents
              , npaths = fromIntegral (length qids) }



-- | Look up what an FPath is mapped to, given a function to
-- access a specific map.
lookupFPath :: (Monad m) => (Filesys -> Map.Map (FPath Absolute) a)
            -> FPath Absolute -> FilesysT m (Maybe a)
lookupFPath f fp = State.gets ((Map.!? fp) . f)

-- | Look up what a Fid is mapped to, given a function to
-- access a specific map.
lookupFid :: (Monad m) => (Filesys -> Map.Map (FPath Absolute) a)
          -> Fid -> FilesysT m (Maybe (FPath Absolute, a))
lookupFid f fid = do
  mfp <- State.gets ((Map.!? fid) . ffids)
  case mfp of
    Nothing -> return Nothing
    Just fp -> fmap (fp,) <$> (lookupFPath f fp)

-- | Read the contents of the file described by an fpath.
-- Note that this fpath is assumed to represent a non-directory file.
readFPath :: (Monad m) => FPath Absolute -> FilesysT m (Maybe BL.ByteString)
readFPath = lookupFPath fcontents

writeFPath :: (Monad m) => String -> FPath Absolute -> FilesysT m ()
writeFPath s fp = State.modify write
  where write sys =
          let bs = toLazyByteString (stringUtf8 s)
              fc = Map.insert fp bs (fcontents sys)
          in sys{fcontents=fc}


-- | Get the children of a directory.
dirChildren :: (Monad m) => FPath Absolute -> FilesysT m (Maybe [FPath Absolute])
dirChildren = lookupFPath ffch

-- | Get the children of a directory, referenced by a fid.
fidChildren :: (Monad m) => Fid
            -> FilesysT m (Maybe (FPath Absolute, [FPath Absolute]))
fidChildren = lookupFid ffch


-- | Add children to a directory.
addChildren :: (Monad m) => FPath Absolute
            -> [FPath Absolute] -> FilesysT m ()
addChildren fp fps = State.modify update
  where update sys =
          let ffch' = Map.alter addOrCreate fp (ffch sys)
          in sys{ffch=ffch'}
        addOrCreate Nothing   = Just fps
        addOrCreate (Just ch) = Just (fps ++ ch)
  

-- | Delete all references to a fid from the fid pool,
-- which includes unregistering it from the list of open fids.
deleteFid :: (Monad m) => Fid -> FilesysT m ()
deleteFid fid =
  -- closeFid fid >>
  State.modify del
  where del sys =
          let fids' = Map.delete fid (ffids sys)
          in sys{ffids=fids'}


-- | Record a fid as being 'opened' by the client.
openFid :: (Monad m) => Fid -> FilesysT m ()
openFid _ = return ()
-- openFid fid = State.modify open
--   where open sys =
--           let ofids' = Map.insert fid 0 (ofids sys)
--           in sys{ofids=ofids'}

-- -- | Get the read offset of an open fid.
-- getOffset :: (Monad m) => Fid -> FilesysT m (Maybe (FPath Absolute, Offset))
-- getOffset = lookupFid ofids

-- -- | Update an open fid's offset.  If the fid isn't open,
-- -- nothing will be changed.
-- setOffset :: (Monad m) => Fid -> Offset -> FilesysT m ()
-- setOffset fid off = State.modify setoff
--   where setoff sys =
--           let ofids' = Map.alter msetoff fid (ofids sys)
--           in sys{ofids=ofids'}
--         msetoff = fmap (const off)


-- -- | Record a fid as having been 'closed' by the client.
-- closeFid :: (Monad m) => Fid -> FilesysT m ()
-- closeFid fid = State.modify close
--   where close sys =
--           let ofids' = Map.delete fid (ofids sys)
--           in sys{ofids=ofids'}

-- -- | Query whether a fid is currently opened.
-- isOpen :: (Monad m) => Fid -> FilesysT m Bool
-- isOpen fid = State.gets openp
--   where openp sys = Map.member fid (ofids sys)


-- | Generate a new path number.
newPathNum :: (Monad m) => FilesysT m Word64
newPathNum = do
  n <- State.gets npaths
  State.modify (incnp n)
  return n
  where incnp n sys = sys{npaths=n+1}


-- | Create a qid with the given type, and with a new path number.
newQid :: (Monad m) => QidType -> FilesysT m Qid
newQid typ = do
  n <- newPathNum
  return (mkqid typ n)

-- | Create a new qid, and bind a fid and a path to it.
bindNewFid :: (Monad m) => Fid -> FPath Absolute
           -> QidType -> FilesysT m Qid
bindNewFid fid fpath typ = do
  putFid fid fpath
  q <- bindNewFPath fpath typ
  return q

-- | Create a new qid, and bind a path to it.
bindNewFPath :: (Monad m) => FPath Absolute -> QidType -> FilesysT m Qid
bindNewFPath fpath typ = do
  q <- newQid typ
  putFPath fpath q
  return q

-- | Bind a fid to a file path, without making a new qid for it.
putFid :: (Monad m) => Fid -> FPath Absolute -> FilesysT m ()
putFid fid fpath = State.modify assocfid
  where assocfid sys =
          let fids' = Map.insert fid fpath (ffids sys)
          in sys{ffids=fids'}

-- | Bind a file path to a qid.
putFPath :: (Monad m) => FPath Absolute -> Qid -> FilesysT m ()
putFPath fpath q = State.modify putfp
  where putfp sys =
          let qids' = Map.insert fpath q (fqids sys)
          in sys{fqids=qids'}

-- | Look up the file path and qid that a fid refers to.
getFidQid :: (Monad m) => Fid -> FilesysT m (Maybe (FPath Absolute, Qid))
getFidQid = lookupFid fqids

-- | Look up the qid that a file path refers to.
getFPathQid :: (Monad m) => FPath Absolute -> FilesysT m (Maybe Qid)
getFPathQid = lookupFPath fqids

-- | Query whether a fid is bound to anything.
fidExists :: (Monad m) => Fid -> FilesysT m Bool
fidExists fid = State.gets (Map.member fid . ffids)

-- | Query whether an fpath is bound to anything.
fpathExists :: (Monad m) => FPath Absolute -> FilesysT m Bool
fpathExists fp = State.gets (Map.member fp . fqids)


-- all fpaths should be completely relative
data FileTree =
  Node (FPath Relative) [FileTree]
  | Leaf (FPath Relative) String
  deriving (Eq, Show)


-- | Add a 'tree' of files to a given FPath.  Will fail
-- if the FPath does not exist within the filesystem.
addTree :: (Monad m) => FPath Absolute -> FileTree -> FilesysT m (Maybe Qid)
addTree place tree = fpathExists place >>= \case
  False -> return Nothing
  True  -> fmap Just (insert place tree)
  where insert at (Leaf fp s) = do
          let fp' = fp `absoluteFrom` at          
          addChildren at [fp']
          writeFPath s fp'
          bindNewFPath fp' FNORMAL
        insert at (Node fp ts) = do
          let fp' = fp `absoluteFrom` at
          addChildren at [fp']
          mapM_ (insert fp') ts
          bindNewFPath fp' FDIR
