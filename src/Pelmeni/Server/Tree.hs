{-# LANGUAGE RecordWildCards #-}

-- | Functions for representing Pleroma API data as a file tree.
module Pelmeni.Server.Tree (ToTree(..)) where

import Pelmeni.Pleroma as P
import Pelmeni.Server.Filesys
import Pelmeni.Server.Path
import Pelmeni.Server.Types
import Pelmeni.Server.Util

import Data.Maybe (fromMaybe, maybe)



-- | Class for things that can be represented as a file tree.
class ToTree a where
  toTree :: a -> Server FileTree

instance ToTree P.Account where
  toTree P.Account{..} =
    return (Node (FPath [fromID handle])
             [ Leaf (FPath ["bio"]) bio
             , Leaf (FPath ["alias"]) username
             , Leaf (FPath ["statuses"]) "" ])


instance ToTree P.Notification where
  toTree P.Notification{..} =
    return (Node (FPath [fromID notifID])
            [ Leaf (FPath ["author"]) author
            , Leaf (FPath ["type"]) (show ntype)
            , Leaf (FPath ["status"]) status ])
    where ID author = getID nacct
          status = maybe "" (fromID . getID) nstatus


-- expiredp:votedp:multiplep:pollID:numVotes:numVoters:clientVotes*\n
-- poll option #1\tX\n
-- poll option #2\tX\n
--       ...     \tX\n
-- poll option #n\tX\n
instance ToTree P.Poll where
  toTree P.Poll{..} =
    return (Leaf (FPath ["poll"]) stringypoll)
    where stringypoll =
            unlines (header : map stropt pollOptions)
          header = colonsep
            (map strbool [expiredp, votedp, multiplep]
             ++ [fromID pollID, show numVotes, show numVoters]
             ++ map show clientVotes)
          stropt (PollOption s n) = s ++ '\t' : show n


-- TODO: consider making status attributes (stars, reblogs, reacts, thread)
-- initially empty, and loaded lazily.  also, there ought to be an option
-- for reloading all of these attributes, to check for updated info.
instance ToTree P.Status where
  toTree P.Status{..} = do
    mpoll <- mapM toTree poll
    sstars <- stars
    sreblogs <- reblogs
    sreacts <- reacts
    sthread <- thread
    return (Node (FPath [fromID statusID])
             (mcons mpoll [ Leaf (FPath ["content"]) content
                          , Leaf (FPath ["media"]) urls
                          , Leaf (FPath ["author"]) op
                          , Leaf (FPath ["stars"]) sstars
                          , Leaf (FPath ["reblogs"]) sreblogs
                          , Leaf (FPath ["reacts"]) sreacts
                          , Leaf (FPath ["thread"]) sthread ]))
    where urls = unlines attachments
          ID op = handle (fromMaybe author rebloggedFrom)
          stars = do
            mres <- pleromaReq (getFavorites statusID)
            let accts = listIDs (fromMaybe [] mres)
            return accts
          reblogs = do
            mres <- pleromaReq (getRepeats statusID)
            let accts = listIDs (fromMaybe [] mres)
            return accts
          reacts = do
            mrcts <- pleromaReq (getReactions statusID)
            let rcts = fromMaybe [] mrcts
            let rs = (flip map) rcts $ \P.Reaction{..} ->
                  colonsep [ emoji, show count
                           , strbool clientReacted
                           , listIDs reactors ]
            return (unlines rs)
          -- thread = return ""
          thread = do
            mctx <- pleromaReq (getContext statusID)
            case mctx of
              Nothing -> return ""
              Just P.ThreadContext{..} ->
                return (unlines [ listIDs thancestors
                                , listIDs thdescendants ])
