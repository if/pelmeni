{-# LANGUAGE OverloadedStrings #-}

-- | Contains functionality for parsing configuration data.
module Pelmeni.Config
  ( Config(..)
  , parseConfig ) where

import Data.Aeson ( FromJSON(..)
                  , withObject
                  , (.:)
                  , eitherDecodeFileStrict' )



data Config =
  Config { instance_ :: String
         -- ^ pleroma instance that the client should use
         , token :: String
         -- ^ user's API token
         , port :: Int
         -- ^ the port on which to run the (local) IRC server
         } deriving (Show)


instance FromJSON Config where
  parseJSON = withObject "Config" parse
    where parse v = do
            i <- v .: "instance"
            t <- v .: "token"
            p <- v .: "port"
            return Config{ instance_ = i
                         , token = t
                         , port = p  }


-- | Parse a JSON configuration file, returning either
-- an error message, or a @Config@.  This function is strict.
parseConfig :: FilePath -> IO (Either String Config)
parseConfig = eitherDecodeFileStrict'
