{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}


-- | Contains functions for interacting with a pleroma instance.
module Pelmeni.Pleroma where

import Control.Monad (mapM)

import Data.List (intercalate)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C8
import Data.Aeson ( FromJSON(..)
                  , Value
                  , object
                  , withObject
                  , withText
                  , (.:)
                  , (.:?)
                  , (.=) )

import GHC.Generics

import Network.HTTP.Client.MultipartFormData
import Network.HTTP.Simple

import System.IO (FilePath)





-- | HTTP request method.
data HTTPMethod =
  GET
  | POST
  | DELETE
  | PATCH
  | HEAD
  | PUT
  | CONNECT
  | OPTIONS
  | TRACE
  deriving (Eq, Ord, Show)


-- | An authorization token.
type Token = B.ByteString

-- | A URI or component thereof.
type URI = String

-- | A user's session.
data Session =
  Session { instance_ :: URI, token :: Token }
  deriving (Eq, Show)

-- | Helper function for making sessions; takes strings as input.
makeSession :: String -> String -> Session
makeSession inst t =
  Session { instance_ = inst
          , token = C8.pack t }


-- | An identifier for a status, user, &c.  The type parameter describes
-- what sort of object the identifier represents; for example, @ID Status@
-- is an identifier for a status.
newtype ID a = ID String
  deriving (Eq, Generic, Ord, Show)

fromID :: ID a -> String
fromID (ID a) = a

instance FromJSON (ID a)

class HasID a where
  getID :: a -> ID a


-- | Specifies a range of objects, with a minimum and maximum ID.
-- Pleroma API calls tend to think of these ranges as non-inclusive,
-- i.e. (min_id, max_id) rather than [min_id, max_id].
type IDRange a = (ID a, ID a)


-- | A unit value that represents an 'ignored' API response;
-- any API response can be parsed into @Unit@.
data Unit = Unit deriving (Eq, Show)
-- | Unlike @()@, anything parses to @Unit@
instance FromJSON Unit where
  parseJSON = withObject "Unit" (\_ -> return Unit)



-- TODO: more can be added, such as the timestamp, number of
-- favorites/repeats, bookmarked-p, &c 
-- | Also known as a 'post'.
data Status = Status
  { author :: Account
  , content :: String
  , poll :: Maybe Poll
  , statusID :: ID Status
  , replyingTo :: Maybe (ID Account)
  , rebloggedFrom :: Maybe Account
  , attachments :: [String] }
  deriving (Eq, Show)

instance HasID Status where
  getID = statusID


-- TODO: clean this up
instance FromJSON Status where
  parseJSON = withObject "Status" $ \v -> Status
    <$> v .: "account"
    <*> (do { c <- v .: "content"
            ; return c })
    <*> getPoll v
    <*> v .: "id"
    <*> (v .: "pleroma" >>= (.:? "in_reply_to_account_acct"))
    <*> (do { r <- v .:? "reblog";
              case r of
                Just r' -> do
                  a <- r' .: "account"
                  return (Just a)
                Nothing -> return Nothing })
    <*> (do { as <- v .: "media_attachments"
            ; mapM (.: "url") as })
    where getPoll v = do
            p <- v .:? "poll"
            case p of
              Just p' -> return (Just p')
              Nothing -> do
                r <- v .:? "reblog"
                case r of
                  Nothing -> return Nothing
                  Just r' -> r' .:? "poll"



-- | Person A's relationship with person B.  This means that
-- @blocking@ denotes whether person A is blocking person B, and so on.
data Relationship = Relationship
  { blocking :: Bool, following :: Bool, muting :: Bool }
  deriving (Eq, Show)


-- | Two people's relationships with one another.  In the API
-- calls, "person A" is taken to be the client.  This means
-- that @blocking (a2b rel)@ denotes whether the client is blocking
-- person B, while @blocking (b2a rel)@ denotes whether person B is
-- blocking the client.
data DualRelationship = DualRelationship
  { a2b :: Relationship, b2a :: Relationship }
  deriving (Eq, Show)



data Account = Account
  { username :: String
  , handle :: ID Account
  , bio :: String }
  deriving (Eq, Show)

instance HasID Account where
  getID = handle


instance FromJSON Account where
  parseJSON = withObject "Account" $ \v -> Account
    <$> v .: "display_name"
    <*> v .: "acct"
    <*> v .: "note"


-- | A _custom_ emoji.
data Emoji = Emoji (ID Emoji) URI
  deriving (Eq, Show)

instance HasID Emoji where
  getID (Emoji i _) = i

instance FromJSON Emoji where
  parseJSON = withObject "Emoji" $ \v -> Emoji
    <$> v .: "shortcode"
    <*> v .: "static_url"



data Reaction = Reaction
  { emoji :: String
  , count :: Int
  , reactors :: [Account]
  , clientReacted :: Bool }
  deriving (Eq, Show)

instance FromJSON Reaction where
  parseJSON = withObject "Reaction" $ \v -> Reaction
    <$> v .: "name"
    <*> v .: "count"
    <*> v .: "accounts"
    <*> v .: "me"



data PollOption = PollOption String Int
  deriving (Eq, Show)

instance FromJSON PollOption where
  parseJSON = withObject "PollOption" $ \v -> PollOption
    <$> v .: "title"
    <*> v .: "votes_count"

data Poll = Poll
  { expiredp :: Bool
  , pollID :: ID Poll
  , multiplep :: Bool -- whether poll is multiple-choice
  , pollOptions :: [PollOption]
  , clientVotes :: [Int] -- what the user voted if votedp
  , votedp :: Bool
  , numVotes :: Int
  , numVoters :: Int }
  deriving (Eq, Show)

instance HasID Poll where
  getID = pollID

instance FromJSON Poll where
  parseJSON = withObject "Poll" $ \v -> Poll
    <$> v .: "expired"
    <*> v .: "id"
    <*> v .: "multiple"
    <*> v .: "options"
    <*> v .: "own_votes"
    <*> v .: "voted"
    <*> v .: "votes_count"
    <*> v .: "voters_count"


data ThreadContext = ThreadContext
  { thancestors   :: [Status]
  , thdescendants :: [Status] }
  deriving (Eq, Show)

instance FromJSON ThreadContext where
  parseJSON = withObject "ThreadContext" $
    \v -> ThreadContext
          <$> v .: "ancestors"
          <*> v .: "descendants"



newtype Tag = Tag String
  deriving (Eq, Show)



data NotificationType =
  Follow
  | Favourite
  | Reblog
  | Mention
  | EmojiReaction
  | ChatMention
  | Report
  | Move
  | FollowRequest
  | PollNotif
  deriving (Eq, Ord)

instance Show NotificationType where
  show nt = case nt of
    Follow    -> "follow"
    Favourite -> "favourite"
    Reblog    -> "reblog"
    Mention   -> "mention"
    Move      -> "move"
    PollNotif -> "poll"
    FollowRequest -> "follow_request"
    ChatMention   -> "chat_mention"
    Report        -> "pleroma:report"
    EmojiReaction ->
      "pleroma:emoji_reaction"

    

instance FromJSON NotificationType where
  parseJSON = withText "NotificationType" go
    where go s = case s of
            "follow"    -> return Follow
            "favourite" -> return Favourite
            "reblog"    -> return Reblog
            "mention"   -> return Mention
            "move"      -> return Move
            "poll"      -> return PollNotif
            "follow_request" ->
              return FollowRequest
            "pleroma:emoji_reaction" ->
              return EmojiReaction
            "pleroma:chat_mention" ->
              return ChatMention
            "pleroma:report" ->
              return Report
            _ -> fail "unknown notification type"

data Notification = Notification
  { nacct   :: Account
  , notifID :: ID Notification
  , nstatus :: Maybe Status
  , ntype   :: NotificationType }
  deriving (Eq, Show)

instance HasID Notification where
  getID = notifID

instance FromJSON Notification where
  parseJSON = withObject "Notification" $ \v ->
    Notification
    <$> v .: "account"
    <*> v .: "id"
    <*> v .: "status"
    <*> v .: "type"
    


data Payload = PNone | PJSON Value | PFile FilePath
  deriving (Eq, Ord, Show)

-- | An action that can be performed with the Pleroma API.
data Action a = Action
  { endpoint :: URI
  , method :: HTTPMethod
  , payload :: Payload
  , params :: Query }


-- | The visibility scope of a status.
data Scope = Direct | Private | Unlisted | Public
  deriving (Eq, Ord)

instance Show Scope where
  show scope =
    case scope of
      Direct -> "direct"
      Private -> "private"
      Unlisted -> "unlisted"
      Public -> "public"



showBS :: (Show a) => a -> B.ByteString
showBS = C8.pack . show


-- | Build an API endpoint from path components.
buildEndpoint :: [URI] -> URI
buildEndpoint parts = intercalate "/" parts



-- | Set the type of HTTP method that a request uses.
setHTTPMethod :: HTTPMethod -> Request -> Request
setHTTPMethod m = setRequestMethod (showBS m)


-- | Set a request's body.
setBody :: Payload -> Request -> IO Request
setBody (PFile  fp) = formDataBody [partFileSource "file" fp]
setBody (PJSON val) = return . setRequestBodyJSON val
setBody       PNone = return


-- | Build a request from an action along with session info.
buildRequest :: Session -> Action a -> IO (Maybe Request)
buildRequest session action =
  mapM prepare (parseRequest fullURI)
  where fullURI = instance_ session ++ endpoint action
        prepare r =
          r |> setHTTPMethod (method action)
            |> setRequestBearerAuth (token session)
            |> setRequestQueryString (params action)
            |> setBody (payload action)


-- | Build and then perform a request.
performRequest :: (FromJSON a) => Session -> Action a -> IO (Maybe a)
performRequest session action = do
  mreq <- buildRequest session action
  case mreq of
    Nothing -> return Nothing
    Just req -> do
      res <- httpJSONEither req
      return (either (const Nothing) Just (getResponseBody res))





getNotifications :: IDRange Notification -> Action [Notification]
getNotifications (ID min_id, ID max_id) =
  Action { endpoint = "/api/v1/notifications"
         , method = GET
         , payload = PNone
         , params = [ ("with_muted", Just "false")
                    , ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ] }


clearNotifications :: Action Unit
clearNotifications =
  Action { endpoint = "/api/v1/notifications/clear"
         , method = POST
         , payload = PNone
         , params = [] }



getAccount :: ID Account -> Bool -> Action Account
getAccount (ID name) withRels =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name]
         , method = GET
         , payload = PNone
         , params = [("with_relationships", Just withRels')] }
  where withRels' = if withRels then "true" else "false"


-- note: default limit is 20; max limit is 40
-- will return 404 if users' followers are hidden
getFollowing :: ID Account -> Bool -> IDRange Account -> Action [Account]
getFollowing (ID name) withRels (ID min_id, ID max_id) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "following"]
         , method = GET
         , payload = PNone
         , params = [ ("with_relationships", Just withRels')
                    , ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ] }
  where withRels' = if withRels then "true" else "false"

getFollowers :: ID Account -> Bool -> IDRange Account -> Action [Account]
getFollowers (ID name) withRels (ID min_id, ID max_id) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "followers"]
         , method = GET
         , payload = PNone
         , params = [ ("with_relationsips", Just withRels')
                    , ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ]}
  where withRels' = if withRels then "true" else "false"




-- TODO: this doesn't seem to return anything relevant
-- note: you can change this to request data for multiple users at once, if needed
-- | Get the relationship of the logged-in user with the user identified by `name'.
-- Relationships are things like "muted," "following," &c.
getRelationship :: ID Account -> Action Relationship
getRelationship (ID name) =
  Action { endpoint = "/api/v1/accounts/relationships"
         , method = GET
         , payload = PNone
         , params = [ ("id", Just (C8.pack name)) ] }


-- note: there are many query parameters you can use to customize what sort
-- of statuses get returned
getStatuses :: ID Account -> IDRange Status -> Action [Status]
getStatuses (ID name) (ID min_id, ID max_id) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "statuses"]
         , method = GET
         , payload = PNone
         , params = [ ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id))
                    , ("with_muted", Just "false") ] }


writePost :: String -> Action Unit
writePost post =
  Action { endpoint = "/api/v1/statuses"
         , method = POST
         , payload =
             PJSON (object [ "status" .= post
                           , "visibility" .= ("unlisted" :: String) ])
         , params = [] }

mute :: ID Account -> Action Unit
mute (ID name) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "mute"]
         , method = POST
         , payload = PJSON (object [])
         , params = [] }

unmute :: ID Account -> Action Unit
unmute (ID name) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "unmute"]
         , method = POST
         , payload = PJSON (object [])
         , params = [] }

block :: ID Account -> Action Unit
block (ID name) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "block"]
         , method = POST
         , payload = PJSON (object [])
         , params = [] }

unblock :: ID Account -> Action Unit
unblock (ID name) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "unblock"]
         , method = POST
         , payload = PJSON (object [])
         , params = [] }

follow :: ID Account -> Bool -> Action Unit
follow (ID name) showReblogs =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "follow"]
         , method = POST
         , payload = PJSON (object [ "reblogs" .= showReblogs ])
         , params = [] }

unfollow :: ID Account -> Action Unit
unfollow (ID name) =
  Action { endpoint = buildEndpoint ["/api/v1/accounts", name, "unfollow"]
         , method = POST
         , payload = PJSON (object [])
         , params = [] }

-- test if the client's credentials are accepted; 200 -> yes, 422 -> unauthorized
verifyCredentials :: Action Unit
verifyCredentials =
  Action { endpoint = "/api/v1/apps/verify_credentials"
         , method = GET
         , payload = PNone
         , params = [] }


customEmojis :: Action [Emoji]
customEmojis =
  Action { endpoint = "/api/v1/custom_emojis"
         , method = GET
         , payload = PNone
         , params = [] }

-- note: this downloads a zip file--don't use `httpJSON'
downloadEmojis :: String -> Action Unit
downloadEmojis pack =
  Action { endpoint = "/api/v1/pleroma/emoji/packs/archive"
         , method = GET
         , payload = PNone
         , params = [("name", Just (C8.pack pack))]}

reportUser :: ID Account -> String -> Bool -> [String] -> Action Unit
reportUser (ID acct) reason forwardp posts =
  Action { endpoint = "/api/v1/reports"
         , method = POST
         , payload = PJSON (object [ "account_id" .= acct
                                   , "comment" .= reason
                                   , "forward" .= forwardp
                                   , "status_ids" .= posts ])
         , params = [] }

emojiReact :: ID Status -> String -> Action Unit
emojiReact (ID post) e =
  Action { endpoint = buildEndpoint [ "/api/v1/pleroma/statuses"
                                    , post, "reactions", e ]
         , method = PUT
         , payload = PNone
         , params = [] }

-- note: this is configured to get reactions from muted accounts as well
getReactions :: ID Status -> Action [Reaction]
getReactions (ID post) =
  Action { endpoint = buildEndpoint [ "/api/v1/pleroma/statuses", post, "reactions" ] 
         , method = GET
         , payload = PNone
         , params = [] }


rmReaction :: ID Status -> String -> Action Unit
rmReaction (ID post) e =
  Action { endpoint = buildEndpoint [ "/api/v1/pleroma/statuses"
                                    , post, "reactions", e ]
         , method = DELETE
         , payload = PNone
         , params = [] }


voteOnPoll :: ID Status -> [Int] -> Action Unit
voteOnPoll (ID status) choices =
  Action { endpoint = buildEndpoint ["/api/v1/polls", status, "votes"]
         , method = POST
         , payload = PJSON (object ["choices" .= choices])
         , params = [] }


publicTimeline :: IDRange Status -> Action [Status]
publicTimeline (ID min_id, ID max_id) =
  Action { endpoint = "/api/v1/timelines/public"
         , method = GET
         , payload = PNone
         , params = [ ("with_muted", Just "false")
                    , ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ] }

localTimeline :: IDRange Status -> Action [Status]
localTimeline range =
  let publicTL = publicTimeline range
      publicParams = params publicTL
  in publicTL { params = ("local", Just "true") : publicParams }

-- note: the pleroma api allows a lot more complex hashtag searches
-- to be performed; it would be tricky to implement in a tui, and
-- perhaps pointless since federated search is difficult, but just
-- remember that it's an option.
-- IMPORTANT: `tag' represents the tag _without_ its # prefix
hashtagTimeline :: Tag -> IDRange Status -> Action [Status]
hashtagTimeline (Tag tag) (ID min_id, ID max_id) =
  Action { endpoint = buildEndpoint ["/api/v1/timelines/tag", tag]
         , method = GET
         , payload = PNone
         , params = [ ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ]}

homeTimeline :: IDRange Status -> Action [Status]
homeTimeline (ID min_id, ID max_id) =
  Action { endpoint = "/api/v1/timelines/home"
         , method = GET
         , payload = PNone
         , params = [ ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ] }


getBookmarks :: IDRange Status -> Action [Status]
getBookmarks (ID min_id, ID max_id) =
  Action { endpoint = "/api/v1/bookmarks"
         , method = GET
         , payload = PNone
         , params = [ ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ] }

getDMs :: IDRange Status -> Action [Status]
getDMs (ID min_id, ID max_id) =
  Action { endpoint = "/api/v1/timelines/direct"
         , method = GET
         , payload = PNone
         , params = [ ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id)) ] }

searchAccounts :: String -> Action [Account]
searchAccounts query =
  Action { endpoint = "/api/v1/accounts/search"
         , method = GET
         , payload = PNone
         , params = [ ("q", Just (C8.pack query)) ] }


searchStatuses :: String -> IDRange Status -> Action [Status]
searchStatuses query (ID min_id, ID max_id) =
  Action { endpoint = "/api/v1/search"
         , method = GET
         , payload = PNone
         , params = [ ("q", Just (C8.pack query))
                    , ("min_id", Just (C8.pack min_id))
                    , ("max_id", Just (C8.pack max_id))
                    , ("type", Just "statuses") ] }

getFavorites :: ID Status -> Action [Account]
getFavorites (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "favourited_by"]
         , method = GET
         , payload = PNone
         , params = [] }

getRepeats :: ID Status -> Action [Account]
getRepeats (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "reblogged_by"]
         , method = GET
         , payload = PNone
         , params = [] }

getStatus :: ID Status -> Action Status
getStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status]
         , method = GET
         , payload = PNone
         , params = [ ("with_muted", Just "false") ] }

getContext :: ID Status -> Action ThreadContext
getContext (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "context"]
         , method = GET
         , payload = PNone
         , params = [] }

favoriteStatus :: ID Status -> Action Unit
favoriteStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "favourite"]
         , method = POST
         , payload = PNone
         , params = [] }

unfavoriteStatus :: ID Status -> Action Unit
unfavoriteStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "unfavourite"]
         , method = POST
         , payload = PNone
         , params = [] }

repeatStatus :: ID Status -> Action Unit
repeatStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "reblog"]
         , method = POST
         , payload = PJSON (object [ "visibility" .= ("public" :: String) ])
         , params = [] }

unrepeatStatus :: ID Status -> Action Unit
unrepeatStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "unreblog"]
         , method = POST
         , payload = PNone
         , params = [] }

pinStatus :: ID Status -> Action Unit
pinStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "pin"]
         , method = POST
         , payload = PNone
         , params = [] }

unpinStatus :: ID Status -> Action Unit
unpinStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "unpin"]
         , method = POST
         , payload = PNone
         , params = [] }

muteConvo :: ID Status -> Action Unit
muteConvo (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "mute"]
         , method = POST
         , payload = PNone
         , params = [] }

unmuteConvo :: ID Status -> Action Unit
unmuteConvo (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "unmute"]
         , method = POST
         , payload = PNone
         , params = [] }

bookmarkStatus :: ID Status -> Action Unit
bookmarkStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "bookmark"]
         , method = POST
         , payload = PNone
         , params = [] }

unbookmarkStatus :: ID Status -> Action Unit
unbookmarkStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status, "unbookmark"]
         , method = POST
         , payload = PNone
         , params = [] }

deleteStatus :: ID Status -> Action Unit
deleteStatus (ID status) =
  Action { endpoint = buildEndpoint ["/api/v1/statuses", status]
         , method = DELETE
         , payload = PNone
         , params = [] }

-- TODO: support multiple MIME types, as well as other options (see docs)
-- TODO: parameters are way too messy--clean it all up
-- | The well-spring of rudeness and debauchery.
postStatus :: (ID Status) -> String -> String ->
              [String] -> Scope -> Bool -> Action (ID Status)
postStatus (ID reply) spoiler status media scope sensitivep =
  Action { endpoint = "/api/v1/statuses"
         , method = POST
         , payload = PJSON (object pload)
         , params = [] }
  where pload = [ ("status" .= status)
                , ("visibility" .= show scope)
                , ("sensitive" .= sensitivep)
                , ("spoiler_text" .= spoiler)
                , ("media_ids" .= media )
                , ("in_reply_to_id" .= reply) ]


-- see https://docs.joinmastodon.org/methods/statuses/media/
data Attachment =
  Attachment { attachid  :: ID Attachment
             , attachurl :: String }
  deriving (Eq, Ord, Show)

instance HasID Attachment where
  getID = attachid

instance FromJSON Attachment where
  parseJSON = withObject "Attachment" $ \v ->
    Attachment
    <$> v .: "id"
    <*> v .: "url"

uploadMedia :: FilePath -> Action Attachment
uploadMedia fp =
  Action { endpoint = "/api/v1/media"
         , method = POST
         , payload = PFile fp
         , params = [] }
          




-- some utility functions
-- | Flipped version of ($) to enhance code readability.
(|>) :: a -> (a -> b) -> b
(|>) = flip ($)

-- see @foldMaybe@ in package `ghc'.
-- | @foldMaybe f a b@ applies @f@ to @b@ if @a@ is @Just@, and
-- returns @b@ unchanged if @a@ is @Nothing@.
foldMaybe :: (a -> b -> b) -> Maybe a -> b -> b
foldMaybe f (Just a) b = f a b
foldMaybe _  Nothing b = b
