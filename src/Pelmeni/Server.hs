{-# LANGUAGE OverloadedStrings #-}

-- | A 9P server that acts as an interface to pleroma.
module Pelmeni.Server (startServer) where

import qualified Pelmeni.Pleroma as P
import Pelmeni.Server.Util
import Pelmeni.Server.ReadWrite as RW
import qualified Pelmeni.Config as C
import Pelmeni.Server.Path
import Pelmeni.Server.Filesys
import Pelmeni.Server.Types

import Control.Monad.IO.Class (liftIO)

import Data.Binary.Get as G
import Data.Binary.Put (runPut)
import qualified Data.ByteString.Lazy.Char8 as CL
import Data.Maybe (maybe)
import Data.NineP

import Network.Run.TCP (runTCPServer)
import Network.Socket (Socket)
import Network.Socket.ByteString.Lazy (recv, sendAll)

import Control.Exception


data RError =
  EUnsupported Msg
  | EBadFid Fid
  | EBadPath (FPath Absolute)
  | EIO
  deriving (Eq, Show)


-- | Get a human-readable summary of an error.
errmsg :: RError -> String
errmsg (EUnsupported msg) = "unsupported T-message: " ++ show msg
errmsg (EBadFid fid) = "bad fid: " ++ show fid
errmsg (EBadPath fp) = "bad path: " ++ joinpath fp
errmsg EIO = "input/output error"



-- represents a synthetic file system with some initial files set up.
fsys :: Filesys
fsys = initFilesys
   [ (fproot, mkqid FDIR 0)
   , (_people, mkqid FDIR 1)
   , (_star,   mkqid FNORMAL 2)
   , (_unstar, mkqid FNORMAL 3)
   , (_reblog, mkqid FNORMAL 4)
   , (_unblog, mkqid FNORMAL 5)
   , (_react,  mkqid FNORMAL 6)
   , (_unreact, mkqid FNORMAL 7)
   , (_statuses, mkqid FDIR 8)
   , (_home, mkqid FNORMAL 9)
   , (_local, mkqid FNORMAL 10)
   , (_public, mkqid FNORMAL 11)
   , (_vote, mkqid FNORMAL 12)
   , (_notifications, mkqid FDIR 13)
   , (_notifs, mkqid FNORMAL 14)
   , (_post, mkqid FNORMAL 15) ]
  [(fproot, [ _people, _statuses, _star, _unstar
            , _reblog , _unblog, _react, _unreact
            , _home, _local, _public, _vote, _notifs
            , _notifications, _post ])]
  (map initEmpty
    [ _star, _unstar, _reblog, _unblog, _react
    , _unreact, _home, _local, _public, _vote
    , _notifs, _notifications, _post ])
  where _people = FPath ["people"]
        _star = FPath ["star"]
        _unstar = FPath ["unstar"]
        _reblog = FPath ["reblog"]
        _unblog = FPath ["unblog"]
        _react = FPath ["react"]
        _unreact = FPath ["unreact"]
        _statuses = FPath ["statuses"]
        _home = FPath ["home"]
        _local = FPath ["local"]
        _public = FPath ["public"]
        _vote = FPath ["vote"]
        _notifs = FPath ["notifs"]
        _notifications = FPath ["notifications"]
        _post = FPath ["post"]
        initEmpty f = (f, "")
  

startServer :: C.Config -> IO ()
startServer cfg = do
  let p = show (C.port cfg)
  putStrLn (":: running 9p server on tcp!localhost!" ++ p)
  runTCPServer Nothing p run
    where run s = runServer cfg fsys (server s) `catch`
            \e -> do putStr "^^^ERROR! "
                     print (e :: SomeException)


-- note: sometimes the client sends multiple requests
-- (T-messages) at once, so the server reads a list of
-- T-messages and replies to them in FIFO order.
-- | Listen on a socket and respond to client requests.
server :: Socket -> Server ()
server s = srvloop
  where srvloop = do
          msgs <- liftIO (recv9P s)
          mapM_ (handleMsg s) msgs
          srvloop

-- | Serialize a R-message, and then send it.
respond9P :: Socket -> Msg -> IO ()
respond9P s msg = sendAll s (runPut (put msg))

-- | Like @recv@, but parses the message into a @Msg@.
recv9P :: Socket -> IO [Msg]
recv9P s = do
  bytes <- recv s 1024
  return (G.runGet getmsgs bytes)
  where getmsgs = do
          empty <- G.isEmpty
          if empty then return []
            else do m  <- get
                    ms <- getmsgs
                    return (m:ms)



handleMsg :: Socket -> Msg -> Server ()
handleMsg s msg@(Msg ty tag b) = do
  r <- resp
  liftIO (respond9P s r)
  return ()
  where
    resp = case (ty, b) of
      (TTversion, Tversion sz v) -> rversion tag sz v
      (TTattach, Tattach fid _ _ _) -> rattach tag fid
      (TTstat, Tstat fid) -> rstat tag fid
      (TTwalk, Twalk f nf wns) -> rwalk tag f nf wns
      (TTopen, Topen f m) -> ropen tag f m
      (TTread, Tread f off n) -> rread tag f off n
      (TTwrite, Twrite fid _ dat) -> rwrite tag fid dat
      (TTclunk, Tclunk f) -> rclunk tag f
      _ -> rerror tag (EUnsupported msg)


-- | Try to create a message by reading a fid, which should
-- represent a file that exists.  If fid doesn't represent
-- an existing file, then return an Rerror message.
withFid :: TagNum -> Fid -> ((FPath Absolute, Qid) -> Server Msg) -> Server Msg
withFid tag fid fun = do
  mfq <- getFidQid fid
  maybe (rerror tag (EBadFid fid)) fun mfq


withFPath :: TagNum -> FPath Absolute -> (Qid -> Server Msg) -> Server Msg
withFPath tag fp fun = do
  mq <- getFPathQid fp
  maybe (rerror tag (EBadPath fp)) fun mq



rwrite :: TagNum -> Fid -> CL.ByteString -> Server Msg
rwrite tag fid dat = withFid tag fid $ \(fp, _) ->
  let dat' = cl2utf8trim dat
      hndl = chooseHandler fp
  in case hndl of
       Just h -> h dat' >> return writeSuccess
       _ -> rerror tag EIO
  where chooseHandler f@(FPath fp) = case fp of
          ["react"]   -> Just RW.react
          ["unreact"] -> Just RW.unreact
          ["star"]    -> Just RW.star
          ["unstar"]  -> Just RW.unstar
          ["reblog"]  -> Just RW.reblog
          ["unblog"]  -> Just RW.unblog
          ["vote"]    -> Just RW.vote
          ["post"]    -> Just RW.post
          ["home"] ->
            Just (RW.tlScroll f (FPath ["statuses"]) P.homeTimeline)
          ["local"] ->
            Just (RW.tlScroll f (FPath ["statuses"]) P.localTimeline)
          ["public"] ->
            Just (RW.tlScroll f (FPath ["statuses"]) P.publicTimeline)
          ["notifs"] ->
            Just (RW.tlScroll f (FPath ["notifications"])
                  P.getNotifications)
          ["people", u, "statuses"] ->
            Just (RW.tlScroll f (FPath ["statuses"])
                  (P.getStatuses (P.ID u)))
          _ -> Nothing
        writeSuccess = Msg TRwrite tag (Rwrite (cllen dat))
          


rread :: TagNum -> Fid -> Offset -> Count -> Server Msg
rread tag fid off count = withFid tag fid $ \(fpath, q) -> do
  mrepr <- fileRepr fpath q
  maybe (rerror tag EIO) msg mrepr
  where msg d = return (Msg TRread tag (Rread (trimLBS (off, count) d)))


-- close the fid being clunked, and remove it from the fid->fpath map.
rclunk :: TagNum -> Fid -> Server Msg
rclunk tag fid = do
  deleteFid fid
  return (Msg TRclunk tag Rclunk)

ropen :: TagNum -> Fid -> Mode -> Server Msg
ropen tag fid _ = withFid tag fid $ \(_, q) -> do
  openFid fid
  return (msg q)
  where msg q = Msg TRopen tag (Ropen q 0)


-- TODO: validate input; see man pages for more info
rwalk :: TagNum -> Fid -> Fid -> [String] -> Server Msg
rwalk tag fid nfid wnames = withFid tag fid $ \(fp, _) -> do
  qids <- walk fp (FPath wnames) nfid
  return (Msg TRwalk tag (Rwalk qids))

rerror :: TagNum -> RError -> Server Msg
rerror tag e = return msg
  where msg = Msg TRerror tag (Rerror (errmsg e))

rversion :: TagNum -> Size -> Version -> Server Msg
rversion tag sz v = return msg
  where msg = Msg TRversion tag (Rversion sz v)


rattach :: TagNum -> Fid -> Server Msg
rattach tag fid = do
  -- create the root directory and return its qid
  withFPath tag fproot $ \q -> do
    putFid fid fproot
    return (msg q)
  where msg q = Msg TRattach tag (Rattach q)

rstat :: TagNum -> Fid -> Server Msg
rstat tag fid = withFid tag fid $ \(fp, q) -> do
    stat <- mkstat fp q
    return (Msg TRstat tag (Rstat [stat]))
