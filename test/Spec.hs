{-# LANGUAGE OverloadedStrings #-}

import Control.Arrow (second)
import Control.Monad (mapM_)
import System.Exit (exitFailure)
import Pelmeni.Pleroma
import Pelmeni.Parse as P


main :: IO ()
main = do
  let session = Session
        { instance_ = "https://dummy.instance"
        , token = "dummy_token"}
  -- this tests to see if the following actions can get built into requests
  -- without failing; it doesn't execute them as https requests.
  summarize session [ -- ("getAccount", getAccount "lain" True)
                    -- , ("getFollowing", getFollowing "lain" False 0 20)
                    -- , ("getRelationship", getRelationship "lain")
                    -- , ("getStatuses", getStatuses "lain" 0 1)
                    -- , ("writePost", writePost "#cofe")
                    -- , ("mute", mute "lain")
                    -- , ("unmute", unmute "lain")
                    -- , ("block", block "lain")
                    -- , ("unblock", unblock "lain")
                    -- , ("follow", follow "lain" False)
                    -- , ("unfollow", unfollow "lain")
                    -- , ("verifyCredentials", verifyCredentials)
                    -- , ("downloadEmojis", downloadEmojis "blobcats")
                    -- , ("reportUser", reportUser "lain" "nothing personnel ;-;" True [])
                    -- , ("emojiReact", emojiReact "dummy post" "😳")
                    -- , ("getReactions", getReactions "dummy post")
                    -- , ("rmReaction", rmReaction "dummy post" "😳")
                    -- , ("publicTimeline", publicTimeline All 0 20)
                    -- , ("localTimeline", localTimeline Self 0 10)
                    -- , ("hashtagTimeline", hashtagTimeline "cofe" 0 1)
                    -- , ("homeTimeline", homeTimeline Following 0 2)
                    -- , ("getBookmarks", getBookmarks 0 1)
                    -- , ("getDMs", getDMs 0 1)
                    -- , ("searchAccounts", searchAccounts "archillect")
                    -- , ("searchStatuses", searchStatuses "moce" 0 1)
                    -- , ("getFavorites", getFavorites "dummy post")
                    -- , ("getRepeats", getRepeats "dummy post")
                    -- , ("getStatus", getStatus "dummy post")
                    -- , ("getContext", getContext "dummy post")
                    -- , ("favoriteStatus", favoriteStatus "dummy post")
                    -- , ("unfavoriteStatus", unfavoriteStatus "dummy post")
                    -- , ("repeatStatus", repeatStatus "dummy post")
                    -- , ("unrepeatStatus", unrepeatStatus "dummy post")
                    -- , ("pinStatus", pinStatus "dummy post")
                    -- , ("unpinStatus", unpinStatus "dummy post")
                    -- , ("muteConvo", muteConvo "dummy post")
                    -- , ("unmuteConvo", unmuteConvo "dummy post")
                    -- , ("bookmarkStatus", bookmarkStatus "dummy post")
                    -- , ("unbookmarkStatus", unbookmarkStatus "dummy post")
                    -- , ("deleteStatus", deleteStatus "dummy post")
                    -- , ("postStatus", postStatus (ID "") "" "#cofe" [] Private True)
                    -- , ("listChats", listChats 0 20)
                    ]
  


expect :: (Show a, Eq a) => String -> a -> a -> IO ()
expect name a b
  | a == b = putStrLn ("[PASS] " ++ name)
  | otherwise = do
      putStrLn ("[FAIL] " ++ name)
      putStrLn "not equal:"
      putStr "> " >> print a
      putStr "> " >> print b
      exitFailure


-- summarize :: Session -> [(B.ByteString, Maybe Request)] -> IO ()
summarize session tests =
  mapM_ (go . second (buildRequest session)) tests
  where
    go (name, Just  _) = putStrLn ("[PASS] " ++ name)
    go (name, Nothing) = putStrLn ("[FAIL] " ++ name) >> exitFailure
  
